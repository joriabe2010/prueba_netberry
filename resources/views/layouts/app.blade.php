<html>
    <head>
        <title>Netberry Test</title>
        @livewireStyles
        @vite('resources/css/app.css')
    </head>
    <body> 
        <div class="container">
            @yield('content')
        </div>
        @livewireScripts
    </body>
</html>