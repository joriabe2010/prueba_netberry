<div>
    <div class="flex flex-col max-w-3xl m-12">
        <h1 class="text-3xl font-bold">Gestor de tareas</h1>
        <div class="flex flex-row border-t-2">
            <div class="py-1 w-1/2 py-6 px-2">
                <span class="px-1 text-sm text-gray-600">Tarea</span>
                <input wire:model="task_name" placeholder="Nueva Tarea" type="text" class="text-md block px-3 py-1 rounded-lg w-full bg-white border-2 border-gray-300 placeholder-gray-600 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-gray-600 focus:outline-none">
            </div>
            @foreach ($categories as $key => $category)
                <div class="flex justify-start py-6 px-2 my-auto">
                    <label class="block text-gray-500 font-bold my-4 flex items-center">
                        <input wire:model="selected_categories" class="leading-loose text-pink-600 top-0" type="checkbox" value="{{ $key }}"/>
                        <span class="ml-2 text-sm py-2 text-gray-600 text-left">{{ $category }}</span>
                    </label>
                </div>                
            @endforeach
            <div class="flex justify-start py-6 px-2 my-auto">
                <button wire:click="save()" class="h-8 w-24 px-6 text-lg font-semibold bg-gray-800 w-full text-white rounded-lg block shadow-xl hover:text-white hover:bg-black">
                    Añadir
                </button>
            </div>
        </div>
        <div class="flex flex-col">
            @error('task_name') <span class="text-sm text-red-500 p-1">{{ $message }}</span> @enderror
            @error('selected_categories') <span class="text-sm text-red-500 p-1">{{ $message }}</span> @enderror
        </div>
        <div class="flex flex-col">
            <div class="overflow-x-auto sm:mx-0.5 lg:mx-0.5">
              <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                <div class="overflow-hidden">
                  <table class="min-w-full">
                    <thead class="bg-white border-b">
                      <tr>
                        <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                            Tarea
                        </th>
                        <th scope="col" class="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                            Categorias
                        </th>
                        <th scope="col" class="text-sm text-center font-medium text-gray-900 px-6 py-4">
                            Acciones
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            <tr class="bg-gray-100 border-b">
                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                    {{ $task->task_name }}
                                </td>
                                <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                    @foreach ($task->categories as $category)
                                        <span class="px-2 py-1 bg-gray-300 text-blue-600 rounded"> {{ $category->category_name }}</span>
                                    @endforeach
                                </td>
                                <td class="text-sm text-center text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                    <button wire:click="delete({{ $task->id }})" class="h-6 w-20 mx-auto font-semibold bg-red-500 text-white rounded-lg block shadow-xl hover:text-white hover:bg-red-700">
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        @endforeach                     
                    </tbody>
                  </table>
                  {{ $tasks->links() }}
                </div>
              </div>
            </div>
          </div>
    </div>
</div>
