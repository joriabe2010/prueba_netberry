<?php

namespace App\Http\Livewire\Tasks;

use Livewire\Component;
use App\Models\Category;
use App\Models\Task;
use Livewire\WithPagination;
use Illuminate\Support\Facades\DB;

class Index extends Component
{
    use WithPagination;

    public $categories;

    public array $selected_categories = [];
    public string $task_name = ""; 

    protected $listeners = ['taskAdded' => '$refresh'];

    public function mount(){        
        $this->categories = Category::all()->pluck('category_name', 'id');
    }   

    public function render()
    {
        return view('livewire.tasks.index', ['tasks' => Task::paginate(10)]);
    }

    public function save(){

        $this->validate();        

        DB::transaction(function () {
            $task = new Task;
            $task->task_name = $this->task_name;
            $task->save();

            $task->categories()->attach($this->selected_categories);
        });

        $this->emit('taskAdded');
        $this->clear();
    }

    public function delete($id){

        DB::transaction(function () use ($id) {
            $task = Task::find($id);
            $task->categories()->detach();
            
            $task->delete();
        });

        $this->emit('taskAdded');

    }

    public function clear(){
        $this->selected_categories = [];
        $this->task_name = "";
    }

    protected $rules = [
        'selected_categories' => 'required|min:1',
        'task_name' => 'required|string|min:2|max:100',
    ];

    protected $messages = [
        'selected_categories.required' => '* Debe seleccionar al menos una categoria.',
        'task_name.required' => '* El campo Tarea no puede estar vacio.',
        'selected_categories.min' => '* Debe seleccionar al menos una categoria.',
        'task_name.min' => '* El campo Tarea debe tener al menos 2 caracteres.',
        'task_name.max' => '* El campo Tarea debe tener como maximo 100 caracteres.',
    ];
 
    protected $validationAttributes = [
        'selected_categories' => 'Categorias',
        'task_name' => 'Tarea',

    ];
    
}
