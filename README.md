## Instrucciones
    1.	Clonar repositorio.	
    2.	Ejecutar comandos:
        a.	‘composer install’
        b.  ‘crear archivo .env compiando .env.example’
        c.	‘php artisan migrate’
        d.	‘php artisan db:seed --class=CategorySeeder’
        e.  ‘npm install’
        f.  ‘npm run build’
    3.	Probar el formulario:
        a.	dirigirse a la ruta principal, o
        b.  ejecutar ‘php artisan serve’


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
